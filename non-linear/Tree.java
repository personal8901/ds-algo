public class Tree {
  
  private Node root;  

  public void insert(int val){
     root = insertNode(root,val);
  }

  private Node insertNode(Node root,int val){
    if(root==null){
        return new Node(val);
    }
    else if(val<root.val){
      root.left =  insertNode(root.left, val);
    }else{
      root.right =  insertNode(root.right, val);
    }
      return root;
  }

  public void display(){
    displayNode(root);
    System.out.println();
  }
  private void displayNode(Node root){
      if(root!=null){
          displayNode(root.left);
          System.out.print(root.val+" ");
          displayNode(root.right);
      }
  }

  public void delete(int val){
    root = deleteNode(root,val);
  }

  private Node deleteNode(Node root,int val){

    if(root==null){
      return null;
    }
    else if(val<root.val){
      root.left=deleteNode(root.left, val);
    }else if(val>root.val){
      root.right=deleteNode(root.right, val);
    }else{
      if(root.right==null&&root.left==null){
        return null;
      }
      else if(root.right==null){
        return root.left;
      }
      else if(root.left==null){
        return root.right;
      }else{
        int lowRight = findLow(root.right);
        root.val=lowRight;
        deleteNode(root.right, lowRight);
      }
    }
    return root;
  }
  
  private int findLow(Node curr){
    Node temp =curr;
    while(temp.left!=null){
      temp=temp.left;
    }
    return temp.val;
  }

  public void left(){
     leftView(root,1);
  }
  int level = 0;
  private void leftView(Node curr,int currlevel){
    if(curr==null){
      return;
    }
      if(level<currlevel){
        System.out.print(curr.val+" ");
        level++;
      }
      leftView(curr.left, currlevel+1);
      leftView(curr.right, currlevel+1);
  }
}

class Node{
    int val;
    Node left;
    Node right;
    public Node(int val){
        this.val=val;
    }
}

