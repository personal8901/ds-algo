class Node{
    int data;
    Node next;
}

public class LinkedList {

    Node head; 

    public void insertAtFirst(int data){
        Node node = new Node();
        node.data=data;
        node.next=head;
        head=node;
    }

    public void insertAtLast(int data){
       Node node = new Node();
       node.data=data;
       if(head==null){
           head=node;
           return;
       }
       Node temp=head;
       while(temp.next!=null){
           temp=temp.next;
       }
       temp.next=node;
    }

    public void delete(int data){
        if(head.data==data){
           head=head.next;
        }
        Node prev = head;
        Node temp = head.next;
        while(temp!=null){
            if(temp.data==data && temp.next==null){
              prev.next=null;
            }else if(temp.data==data){
                prev.next=temp.next;
            }
            prev=temp;
            temp=temp.next;
        }
    }

    public boolean search(int data){
        Node temp = head;
        while(temp!=null){
            if(temp.data==data){
                return true;
            }
            temp=temp.next;
        }
        return false;
    }

    public void display(){
        Node temp = head;

        while(temp!=null){
            System.out.print(temp.data + " " );
            temp=temp.next;
        }
        System.out.println();
    }
    
}